/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef AUTH_CONNECTION_H
#define AUTH_CONNECTION_H

#include <stdint.h>
#include <stdbool.h>

#include "auth_interface.h"
#include "softbus_conn_interface.h"

#ifdef __cplusplus
#if __cplusplus
extern "C" {
#endif
#endif

typedef struct {
    uint32_t dataType;
    int32_t module;
    int64_t seq;
    int32_t flag;
    uint32_t len;
} AuthDataHead;

typedef struct {
    void (*onConnectResult)(uint32_t requestId, uint64_t connId, int32_t result, const AuthConnInfo *connInfo);
    void (*onDisconnected)(uint64_t connId, const AuthConnInfo *connInfo);
    void (*onDataReceived)(uint64_t connId, const AuthConnInfo *connInfo, bool fromServer,
        const AuthDataHead *head, const uint8_t *data);
} AuthConnListener;

int32_t AuthConnInit(const AuthConnListener *listener);
void AuthConnDeinit(void);

int32_t ConnectAuthDevice(uint32_t requestId, const AuthConnInfo *connInfo, ConnSideType sideType);
void DisconnectAuthDevice(uint64_t connId);
int32_t PostAuthData(uint64_t connId, bool toServer, const AuthDataHead *head, const uint8_t *data);

ConnSideType GetConnSideType(uint64_t connId);
bool CheckActiveAuthConnection(const AuthConnInfo *connInfo);

const char *GetConnTypeStr(uint64_t connId);
uint32_t GetConnId(uint64_t connId);

#define CONN_INFO "conn[%s:%u]"
#define CONN_DATA(connId) GetConnTypeStr(connId), GetConnId(connId)

#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif
#endif /* AUTH_CONNECTION_H */
